<!DOCTYPE html>
<?php
require_once 'dbo.php';

$config = parse_ini_file('../config.ini');
$domain_url = $config['domain'];

$stripeSessionId = $_GET['session_id'];
$paymentsTable = $config['db_payments_table'];

$dbo = new DBO();
$stmt = $dbo->db->prepare("UPDATE $paymentsTable SET payment_status = 0 WHERE stripe_session_id = :stripeSessionId");
$stmt->bindParam(":stripeSessionId", $stripeSessionId, PDO::PARAM_STR);
$result = $stmt->execute();

$dbo->dbDisconnect();
if (!$result) {
  echo "<script>
  window.location.replace(\"" . $domain_url . "\"
  </script>";
}
?>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Payment Canceled</title>
  <link rel="stylesheet" href="css/normalize.css" />
  <link rel="stylesheet" href="css/global.css" />
</head>

<body>
  <div class="sr-root">
    <div class="sr-main">
      <header class="sr-header">
        <div class="sr-header__logo"></div>
      </header>
      <div class="sr-payment-summary completed-view">
        <h1>Your payment was canceled</h1>
      </div>
    </div>
  </div>
</body>

</html>