<?php
require_once 'shared.php';
require_once 'dbo.php';

$config = parse_ini_file('../config.ini');
$domain_url = $config['domain'];

$checkout_session = \Stripe\Checkout\Session::create([
	'success_url' => $domain_url . '/success.php?session_id={CHECKOUT_SESSION_ID}',
	'cancel_url' => $domain_url . '/canceled.php?session_id={CHECKOUT_SESSION_ID}',
	'payment_method_types' => explode(",", "card"),
	'mode' => 'payment',
	'line_items' => [[
		'price' => $config['price'],
		'quantity' => 1,
	]]
]);

/**
 * Database Payment Statuses
 * 0 - Canceled Stripe Payment
 * 1 - Pending Payment
 * 2 - Completed Payment
 */

$accountId = $body->accountId;
$paymentsTable = $config['db_payments_table'];
$stripeSession = ['sessionId' => $checkout_session['id']];
$stripeSessionId = $stripeSession['sessionId'];


// // Insert "Pending" Stripe payment into transactions table.
$dbo = new DBO();
$stmt = $dbo->db->prepare("INSERT INTO $paymentsTable (account_id, stripe_session_id, payment_status) VALUES (:accountId, :stripeSessionId, '1')");
$stmt->bindParam(":accountId", $accountId, PDO::PARAM_INT);
$stmt->bindParam(":stripeSessionId", $stripeSessionId, PDO::PARAM_STR);
$result = $stmt->execute();

$dbo->dbDisconnect();
if ($result) {
	// Start Checkout Session
	echo json_encode($stripeSession);
} else {
	// Echo error;
	echo "Failed starting payment. Please contact support.";
}
