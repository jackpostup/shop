<!DOCTYPE html>

<?php
if (!isset($_GET['accountId'])) {
  // There needs to be an acount ID get url query
  echo "<h4>Error: There was an issue trying to retrieve your account info. Please contact support.</h4>";
} else {
?>
  <html lang="en">

  <head>
    <meta charset="utf-8" />
    <title data-i18n="title"></title>
    <meta name="description" content="A demo of Stripe Payment Intents" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/global.css" />
    <!-- Load Stripe.js on your website. -->
    <script src="https://js.stripe.com/v3/"></script>
    <script src="./script.js" defer></script>
    <!-- Load translation files and libraries. -->
    <script src="https://unpkg.com/i18next/i18next.js"></script>
    <script src="https://unpkg.com/i18next-xhr-backend/i18nextXHRBackend.js"></script>
    <script src="https://unpkg.com/i18next-browser-languagedetector/i18nextBrowserLanguageDetector.js"></script>
    <script src="./translation.js" defer></script>
  </head>

  <body>
    <div class="sr-root">
      <div class="sr-main">
        <header class="sr-header">
          <div class="sr-header__logo"></div>
        </header>
        <section class="container">
          <div>
            <h1 data-i18n="headline"></h1>
            <h4 data-i18n="subline"></h4>
          </div>

          <input type="hidden" id="quantity-input" min="1" value="1" />


          <input type="hidden" id="accountId" name="accountId" value="<?php echo $_GET['accountId']; ?>" />
          <button id="submit" data-i18n="button.submit" i18n-options='{ "total": "0" }'></button>
        </section>
        <div id="error-message"></div>
      </div>
    </div>
  </body>

  </html>

<?php } ?>