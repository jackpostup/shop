<?php
ini_set('mssql.datetimeconvert', false);

class DBO
{

    public $db = NULL;
    public $dbServer = "";

    public function __construct($RRO = NULL)
    {
        $this->dbConnect();
    }


    public function __destruct()
    {
        //error_log("\nDBO DESTRUNCT\n");
        $this->dbDisconnect();
    }


    //Database connection
    public function dbConnect()
    {
        $config = parse_ini_file(__DIR__ . '/../config.ini');

        try {
            $this->db = null;
            $this->db = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass'], array(PDO::ATTR_PERSISTENT => false));
        } catch (PDOException $e) {
            error_log("Failed to get DB handle: " . $e->getMessage() . "\n" . $config['db_host'] . " " . $config['db_name'] . " " . $config['db_user'] . " " . $config['db_pass'] . " ");
            exit;
        }
    }

    public function GetRecordSet($sqlStr)
    {
        $rs = $this->db->prepare($sqlStr);
        $rs->execute();
        return $rs;
    }

    public function Execute($sqlStr)
    {
        $rs = $this->db->prepare($sqlStr);
        $rs->execute();
    }

    public function FreeRS($rs)
    {
        unset($rs);
    }

    public function dbDisconnect()
    {
        $this->db = null;
        unset($this->db);
    }
}
