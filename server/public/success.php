<!DOCTYPE html>
<?php
require_once 'dbo.php';

$stripeSessionId = $_GET['session_id'];

$config = parse_ini_file('../config.ini');
$domain_url = $config['domain'];
$paymentsTable = $config['db_payments_table'];

// Get Stripe payment data
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => $domain_url . '/get-checkout-session.php?sessionId=' . $stripeSessionId,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));
$response = curl_exec($curl);
curl_close($curl);

$stripeData = json_decode($response);

function hasPendingTransaction($stripeSessionId, $paymentsTable)
{
  $dbo = new DBO();
  $stmt = $dbo->db->prepare("SELECT COUNT(id) as count FROM $paymentsTable WHERE stripe_session_id = :stripeSessionId AND payment_status = 1");
  $stmt->bindParam(":stripeSessionId", $stripeSessionId, PDO::PARAM_STR);
  $stmt->execute();

  $count = 0;
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $count = $row['count'];
  }

  if ($count > 0) {
    return true;
  } else {
    return false;
  }
}

// Prevent Double Loading
if (hasPendingTransaction($stripeSessionId, $paymentsTable)) {
  $dbo = new DBO();
  $stmt = $dbo->db->prepare("
  UPDATE $paymentsTable 
  SET 
  amount_subtotal = :amountSubtotal, 
  amount_total = :amountTotal, 
  stripe_customer_id = :stripeCustomerId, 
  customer_email = :customerEmail, 
  payment_status = 2 
  WHERE stripe_session_id = :stripeSessionId
  ");

  $amountSubtotal = $stripeData->amount_subtotal / 100;
  $amountTotal = $stripeData->amount_total / 100;

  $stmt->bindParam(":amountSubtotal", $amountSubtotal, PDO::PARAM_STR);
  $stmt->bindParam(":amountTotal",  $amountTotal, PDO::PARAM_STR);
  $stmt->bindParam(":stripeCustomerId", $stripeData->customer, PDO::PARAM_STR);
  $stmt->bindParam(":customerEmail", $stripeData->customer_email, PDO::PARAM_STR);
  $stmt->bindParam(":stripeSessionId", $stripeSessionId, PDO::PARAM_STR);
  $result = $stmt->execute();

  if (!$result) {
    echo "<script>
    alert('Something went wrong! Please contact support to get your account loaded.');
    </script>";
  } else {
    // Write Function to Load Account here.
    echo "<script>console.log('Payment Updated in DB.');</script>";
  }
}
?>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Payment Successful</title>
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <link rel="stylesheet" href="css/normalize.css" />
  <link rel="stylesheet" href="css/global.css" />
</head>

<body>
  <div class="sr-root">
    <div class="sr-main">
      <header class="sr-header">
        <div class="sr-header__logo"></div>
      </header>
      <div class="sr-payment-summary completed-view">
        <h1>Payment Successful</h1>
        <h4>
          Your purchase details:</a>
        </h4>
      </div>
      <div class="sr-section completed-view">
        <div class="sr-callout">
          <pre>

              </pre>
        </div>
        <button onclick="window.location.href = '/';">View Shop</button>
      </div>
    </div>
    <div class="sr-content">

    </div>
  </div>

</body>

</html>
<!-- <script>
      var urlParams = new URLSearchParams(window.location.search);
      var sessionId = urlParams.get("session_id")
      if (sessionId) {
        fetch("/get-checkout-session.php?sessionId=" + sessionId).then(function(result){
          return result.json()
        }).then(function(session){
          var sessionJSON = JSON.stringify(session, null, 2);
          document.querySelector("pre").textContent = sessionJSON;
        }).catch(function(err){
          console.log('Error when fetching Checkout session', err);
        });
      }
    </script> -->