ArborenMS Shop

1. Setup Databaes
2. cd server
3. composer install
4. create config.ini file in server folder, and plug in info.

DB:
Name - arborenms
User - root
Pass -

MySql Table

```
CREATE TABLE `arborenms_store_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `stripe_session_id` longtext NOT NULL,
  `amount_subtotal` float(10,2) DEFAULT NULL,
  `amount_total` float(10,2) DEFAULT NULL,
  `stripe_customer_id` varchar(255) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `payment_status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
```
